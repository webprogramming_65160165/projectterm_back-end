import { Module } from '@nestjs/common';
import { MaterialService } from './material.service';
import { MaterialsController } from './material.controller';
import { Material } from './entities/material.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [MaterialsController],
  providers: [MaterialService],
  imports: [TypeOrmModule.forFeature([Material])],
})
export class MaterialModule {}
