import { Injectable } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Material } from './entities/material.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MaterialService {
  constructor(
    @InjectRepository(Material)
    private MaterialsRepository: Repository<Material>,
  ) {}
  create(createMaterialDto: CreateMaterialDto) {
    const material = new Material();
    material.name = createMaterialDto.name;
    if (createMaterialDto.image && createMaterialDto.image !== '') {
      material.image = createMaterialDto.image;
    }
    return this.MaterialsRepository.save(material);
  }

  findAll() {
    return this.MaterialsRepository.find({});
  }

  findOne(id: number) {
    return this.MaterialsRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const material = await this.MaterialsRepository.findOneOrFail({
      where: { id },
    });
    material.name = updateMaterialDto.name;
    if (updateMaterialDto.image && updateMaterialDto.image !== '') {
      material.image = updateMaterialDto.image;
    }
    this.MaterialsRepository.save(material);
    const result = await this.MaterialsRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteMaterial = await this.MaterialsRepository.findOneOrFail({
      where: { id },
    });
    await this.MaterialsRepository.remove(deleteMaterial);

    return deleteMaterial;
  }
}
