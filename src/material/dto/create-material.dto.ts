export class CreateMaterialDto {
  name: string;
  price: string;
  image: string;
  type: string;
}
