import { Injectable } from '@nestjs/common';
import { CreateTypeDto } from './dto/create-type.dto';
import { UpdateTypeDto } from './dto/update-type.dto';
import { Type } from './entities/type.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TypesService {
  constructor(
    @InjectRepository(Type) private TypesRepository: Repository<Type>,
  ) {}
  create(createTypeDto: CreateTypeDto) {
    return this.TypesRepository.save(createTypeDto);
  }

  findAll() {
    return this.TypesRepository.find();
  }

  findOne(id: number) {
    return this.TypesRepository.findOneBy({ id });
  }

  async update(id: number, updateTypeDto: UpdateTypeDto) {
    await this.TypesRepository.findOneByOrFail({ id });
    await this.TypesRepository.update(id, updateTypeDto);
    const updateType = await this.TypesRepository.findOneBy({ id });
    return updateType;
  }

  async remove(id: number) {
    const removeType = await this.TypesRepository.findOneByOrFail({ id });
    await this.TypesRepository.remove(removeType);
    return removeType;
  }
}
