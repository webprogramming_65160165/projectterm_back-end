import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class CheckMaterial {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  bfCheck: number;

  @Column()
  atCheck: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  // @ManyToOne(() => Material, (Material) => Material.checkMaterial, {
  //   onDelete: 'CASCADE',
  // })
  // Material: Material;

  // @ManyToOne(() => CheckStock, (CheckStock) => CheckStock.checkMaterial)
  // CheckStock: CheckStock;
}
