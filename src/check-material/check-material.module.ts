import { Module } from '@nestjs/common';
import { CheckMaterialService } from './check-material.service';
import { CheckMaterialController } from './check-material.controller';

@Module({
  controllers: [CheckMaterialController],
  providers: [CheckMaterialService],
})
export class CheckMaterialModule {}
