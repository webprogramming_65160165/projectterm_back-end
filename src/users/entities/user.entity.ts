//import { CheckStock } from 'src/check-stock/entities/check-stock.entity';
import { join } from 'path';
import { Order } from 'src/orders/entities/order.entity';
import { Role } from 'src/roles/entities/role.entity';

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column({
    default: '',
    name: 'full_name',
  })
  fullName: string;

  @Column({ default: '' })
  gender: string;

  @CreateDateColumn()
  create: Date;

  @UpdateDateColumn()
  update: Date;

  @Column({ default: 'noimages.jpg' })
  image: string;

  @OneToMany(() => Order, (order) => order.user)
  orders: Order[];

  @ManyToMany(() => Role, (roles) => roles.users)
  @JoinTable()
  roles: Role[];

  // @OneToMany(() => CheckStock, (CheckStock) => CheckStock.user)
  // checkStock: CheckStock[];
}
