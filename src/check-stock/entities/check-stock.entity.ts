import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class CheckStock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  checkDate: Date;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  // @OneToMany(() => CheckMaterial, (detail) => detail.CheckStock)
  // checkMaterial: CheckStock[];

  // @ManyToOne(() => User, (user) => user.checkStock, {
  //   onDelete: 'CASCADE',
  //   onUpdate: 'CASCADE',
  // })
  // user: User;
}
