import { Injectable } from '@nestjs/common';
import { CreateCheckStockDto } from './dto/create-check-stock.dto';
import { UpdateCheckStockDto } from './dto/update-check-stock.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CheckStock } from './entities/check-stock.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CheckStockService {
  constructor(
    @InjectRepository(CheckStock)
    private checkStockRepository: Repository<CheckStock>,
  ) {}
  create(createCheckStockDto: CreateCheckStockDto) {
    return this.checkStockRepository.save(createCheckStockDto);
  }

  findAll() {
    return this.checkStockRepository.find({
      relations: [
        'checkMaterialDetails',
        'checkMaterialDetails.Material',
        'user',
      ],
    });
  }

  async getLastId(): Promise<number | undefined> {
    const [latestCheckStock] = await this.checkStockRepository.find({
      order: { id: 'DESC' },
      take: 1,
    });

    return latestCheckStock?.id;
  }

  findOne(id: number) {
    return `This action returns a #${id} checkStock`;
  }

  update(id: number, updateCheckStockDto: UpdateCheckStockDto) {
    return `This action updates a #${id} checkStock`;
  }

  remove(id: number) {
    return `This action removes a #${id} checkStock`;
  }
}
