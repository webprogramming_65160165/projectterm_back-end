import { Module } from '@nestjs/common';
import { CheckStockService } from './check-stock.service';
import { CheckStockController } from './check-stock.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckStock } from './entities/check-stock.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CheckStock])],
  controllers: [CheckStockController],
  providers: [CheckStockService],
})
export class CheckStockModule {}
